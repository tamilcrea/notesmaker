class Todolist < ApplicationRecord

	belongs_to :user


	validates :todoitem, :timesheet, :presence => true


	def todolists
      current_user.todolists.count

	end
end
