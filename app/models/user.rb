class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable

   



 
   

  has_many  :subjects
  has_many  :notes

  has_many  :todolists

  has_many :follower_relationships, class_name: 'Relationship', foreign_key: :follower_id
  has_many :followed_relationships, class_name: 'Relationship', foreign_key: :followed_id


  has_many :followers, through: :followed_relationships, source: :follower

  has_many :followed, through: :follower_relationships, source: :followed



def self.add_public_uid

   User.update(public_uid: SecureRandom.urlsafe_base64(3))

end  

end
