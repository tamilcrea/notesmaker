class SetgoalsController < ApplicationController

	def index
		@setgoals = Setgoal.all
		@setgoal = Setgoal.new
	end


	def create
	  @setgoal = Setgoal.create(setgoal_params)
	end



	private

	def setgoal_params
		params.require(:setgoal).permit(:set_notes)
	end
end
