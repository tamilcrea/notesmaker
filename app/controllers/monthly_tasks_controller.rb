class MonthlyTasksController < ApplicationController

	def index
        @monthly_tasks = MonthlyTask.all
		@monthly_task = MonthlyTask.new
	end

	def create
		@monthly_task = MonthlyTask.create(task_params)
		redirect_to monthly_tasks_path
	end


	private
	def task_params
	 	params.require(:monthly_task).permit(:name, :date)
	end

end
