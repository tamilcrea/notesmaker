class WeeklyNotesController < ApplicationController

	def index 

     date = Date.today 
	 start_date = date.at_beginning_of_week 
     end_date  = date.at_end_of_week  
     dates  = start_date.upto(end_date).to_a
     @weekly_notes = Note.where(date: dates).order('created_at DESC') 	  
	end
end
