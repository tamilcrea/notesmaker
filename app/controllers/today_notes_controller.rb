class TodayNotesController < ApplicationController


	def index
		@today_notes = current_user.notes.where(date: Date.today).order('id DESC')
		@note = Note.new


      respond_to do |format| 

        format.html 
        format.pdf do 

          pdf = NotePdf.new(@today_notes, view_context)

           send_data pdf.render, filename: "notes.pdf",
                                 type: "application/pdf"
                                 
     end 
     end

	end

	def read_notes
		today_notes = current_user.notes.where(date: Date.today)
       @read_notes = today_notes.select { |notes| notes.mark_read.present? }
     
	end

	def update
	 @note = current_user.notes.find(params[:id])
	 @note.update(mark_read: true)
	redirect_to today_notes_path
	end


	def unread_notes
	 today_notes = current_user.notes.where(date: Date.today)
	 @unread_notes = today_notes.where(mark_read: false)
	end

	def mark_read
	 @note = current_user.notes.find(params[:id])
	 @note.update(mark_read: false)
	 redirect_to today_notes_path
   end 
end
