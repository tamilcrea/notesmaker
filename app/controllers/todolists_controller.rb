class TodolistsController < ApplicationController

	def index
		@todolists = current_user.todolists.all.order("created_at Desc")

		@todolist = Todolist.new
	end
	
	def new
         @todolist = Todolist.new
	 end

	 def create
        @todolist = current_user.todolists.create(todo_params)
 
	 end	

	 def edit
         @todolist = current_user.todolists.find(params[:id])
	 end

	 def update
        @todolist = current_user.todolists.find(params[:id])

        @todolist.update(todo_params)

        redirect_to todolists_path

	 end 


	 def destroy
        
        @todolist = current_user.todolists.find(params[:id])

        @todolist.destroy

	 end


	 def delete_all
    
         Todolist.delete_all

         redirect_to todolists_path

	 end


	 private

	 def todo_params
       params.require(:todolist).permit(:todoitem, :timesheet)
	 end	

end
