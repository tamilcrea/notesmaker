class SubjectsController < ApplicationController




	def index
	 @subjects = current_user.subjects.all
	 @Subject = Subject.new
	end


	def create
     @subject = current_user.subjects.create(subject_params)
     redirect_to notes_path  
	end


	def show
      @notes = current_user.notes.where(subject_id: params[:id]).order("created_at DESC")
	end


	private

	def subject_params
	params.require(:subject).permit(:name)
	end

	def downcase_subject
     
     self.name = name.downcase if name.present?

    end  
end
