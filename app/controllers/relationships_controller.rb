class RelationshipsController < ApplicationController


	def index

        @users = current_user.followers.where.not(id: current_user.id).uniq
	end


	def create
 
     @relationship = Relationship.create(follower_id:  params[:follower_id], followed_id: current_user.id)

     redirect_to relationships_path

	end


	def destroy
     
     @relationship = Relationship.find_by(follower_id: params[:id])

     @relationship.destroy

     redirect_to relationships_path

    end


    def followers
    
    @users = current_user.followers.uniq

    end

    def find_users
     
     @user = User.new
     @user = User.find_by(username: params[:username])   
  
    end







end
