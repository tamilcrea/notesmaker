class ConceptsController < ApplicationController

	def create
       
      @note = Note.find_by(id: params[:note_id])

      @concept = @note.concepts.create(concept_params)

   

      redirect_to note_path(@note)
	end


	private

	def concept_params
     params.require(:concept).permit(:name, :desc)
	end


	
	
end
