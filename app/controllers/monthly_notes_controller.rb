class MonthlyNotesController < ApplicationController

	def index

	end


	def show
    date = params[:id]

    new_date = Date.parse(date)

    end_date = new_date.at_end_of_month()

    dates = new_date.upto(end_date).to_a
	
	@monthly_notes = Note.all.where(date: dates).order('created_at DESC')


	end


end
