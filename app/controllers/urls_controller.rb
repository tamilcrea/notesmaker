class UrlsController < ApplicationController

	def index
   
      @urls= Url.all 
	  @url = Url.new	
	end

	def create
	 @url = Url.create(url_params)

	 redirect_to urls_path
	end


	private

	def url_params
		params.require(:url).permit(:url)
	end
end
