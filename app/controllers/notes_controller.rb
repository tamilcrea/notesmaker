class NotesController < ApplicationController


  before_action :set_note, only: [:show, :edit, :update, :destroy]
	
	def index	
	  @notes = current_user.notes.all.order(created_at: :desc)
      @note = current_user.notes.new
      @subject = current_user.subjects.new
      @subjects  = current_user.subjects.all.map { |s| [s.name, s.id]}

      @allsubjects = current_user.subjects.all

      date = Date.today

      start_date = date.at_beginning_of_year

      end_date = date.at_end_of_year

      @dates = start_date.upto(end_date).to_a
  end

    
    def  create
      @note = current_user.notes.create(notes_params)
    end
    
    def show

    end

    def edit
        @subjects  = current_user.subjects.all.map { |s| [s.name, s.id]}
    end

    def update
        @note.update(notes_params)
        binding.pry
        	redirect_to notes_path(@note)
    end
    def destroy
    	 @note.destroy
    end

    def all_notes

      @notes = current_user.notes.all.paginate(page: params[:page],per_page: 10).order('id DESC')

    end

    def unread_notes
    end

    def recent_notes
      @recent_notes = current_user.notes.last(1)
    end 

    def revise_notes
         @note = current_user.notes.find(params[:id])
         @note.update(mark_read: false)
         binding.pry
         redirect_to today_notes_path
    end
   
    private

    def notes_params
      params.require(:note).permit(:topic_name, :notes_date, :notes, :subject_id).merge(date: Date.today, user_id: current_user)
    end

    def set_note
         @note = current_user.notes.find(params[:id])
    end

  
end
