class TodaytasksController < ApplicationController


	def index
		@todaytask = Todaytask.all
		@todaytask = Todaytask.new
	end



	def create
		@todaytask = Todaytask.create(todaytask_params)
	end


	private
	def todaytask_params
		params.require(:todaytask).permit(:start_time, :end_time, :name)
	end
end
