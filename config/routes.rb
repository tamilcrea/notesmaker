Rails.application.routes.draw do
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


  devise_for :users




 resources :subjects

 resources :notes do 
 	resources :concepts 
 end

 root :to => "notes#index"


  get 'all_notes', to: 'notes#all_notes'
  get 'read_notes', to: 'notes#read_notes'
  get 'unread_notes', to: 'notes#unread_notes'
  get 'recent_notes', to: 'notes#recent_notes'

  resources :todolists


  get 'ages', to: 'ages#aptitudes'

  get 'train', to: 'train#aptitudes'

  get 'probability', to: 'probability#aptitudes'

  get 'pipes', to: 'pipes#aptitudes' 

  get 'geomentry', to: 'geomentry#aptitudes' 


  get 'algebra', to: 'algebra#aptitudes'


  get 'static_pages', to: 'static_pages#aptitudes'


  resources :aptitudes

  delete "delete_all", to: 'todolists#delete_all'


  resources :daywise_notes


  resources :relationships

  get 'followers', to: "relationships#followers"

  get 'find_users', to: "relationships#find_users"

  resources :current_updates




 resources :today_notes	
 resources :weekly_notes
 resources :monthly_notes
 resources :urls


 resources :networks


 

 resources :monthly_tasks
 resources :setgoals
 
 resources :mark_notes

 get 'read_today', to: 'today_notes#read_notes'
 get 'unread_today', to: 'today_notes#unread_notes'
 

end
