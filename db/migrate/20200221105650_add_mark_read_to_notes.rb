class AddMarkReadToNotes < ActiveRecord::Migration[5.2]
  def change
    add_column :notes, :mark_read, :boolean, default: false
  end
end
