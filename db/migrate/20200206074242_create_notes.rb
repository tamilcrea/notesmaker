class CreateNotes < ActiveRecord::Migration[5.2]
  def change
    create_table :notes do |t|
      t.text :topic_name
      t.text :notes
      t.date :notes_date
      t.string :attachment
      t.integer :user_id
      
      t.timestamps
    end
  end
end
