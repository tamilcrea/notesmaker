class CreateNetworks < ActiveRecord::Migration[5.2]
  def change
    create_table :networks do |t|
      t.string :name
      t.integer :user_id
      t.integer :subject_id
      t.integer :note_id

      t.timestamps
    end
  end
end
