class CreateTodolists < ActiveRecord::Migration[5.2]
  def change
    create_table :todolists do |t|
      t.text :todoitem

      t.integer :hour
      t.integer :minutes


      t.timestamps
    end
  end
end
