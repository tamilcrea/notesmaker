class CreateTopics < ActiveRecord::Migration[5.2]
  def change
    create_table :topics do |t|
      t.text :topic_name
      t.date :topic_date

      t.timestamps
    end
  end
end
