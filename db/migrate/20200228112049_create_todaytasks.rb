class CreateTodaytasks < ActiveRecord::Migration[5.2]
  def change
    create_table :todaytasks do |t|
      t.time :start_time
      t.time :end_time
      t.text :name

      t.timestamps
    end
  end
end
