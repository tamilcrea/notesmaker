class CreateReviseNotes < ActiveRecord::Migration[5.2]
  def change
    create_table :revise_notes do |t|
      t.references :note, foreign_key: true

      t.timestamps
    end
  end
end
