class CreateSetgoals < ActiveRecord::Migration[5.2]
  def change
    create_table :setgoals do |t|
      t.integer :set_notes

      t.timestamps
    end
  end
end
