class CreateConcepts < ActiveRecord::Migration[5.2]
  def change
    create_table :concepts do |t|
      t.text :name
      t.text :desc
      t.references :note, foreign_key: true

      t.timestamps
    end
  end
end
